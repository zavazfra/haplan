import Dexie, { Table } from 'dexie';
import { CalendarEvent, Note } from './model';

export class HaplanDB extends Dexie {
  calendarList!: Table<CalendarEvent, number>;
  noteList!: Table<Note, number>;

  constructor() {
    super('haplan');
    this.version(1).stores({
      calendarList: '++id, datetime',
      noteList: '++id'
    });
    this.on('populate', () => this.populate());
  }

  async populate() {
    await db.calendarList.add({
      name: 'První záznam v kalendáří',
      place: 'Praha 6',
      datetime: new Date(Date.now()+2000000)
    });
    await db.noteList.add({
      name: 'První poznámka',
      text: ''
    })
    await db.noteList.add({
      name: 'Druhá poznámka',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '+
      'Mauris faucibus, ex eu fermentum vehicula, lectus tellus suscipit enim, eget molestie massa dui et est. '+
      'Etiam interdum volutpat ipsum, non dictum nulla blandit a. Donec interdum suscipit nisl. Nullam finibus ex elit, '+
      'vitae scelerisque quam sollicitudin sit amet. Nullam tempor metus leo, id egestas tortor faucibus et. Suspendisse '+
      'maximus sed leo ut pellentesque. Ut tempor felis non efficitur malesuada. Sed in tristique augue.'
    })
  }


  // calendar methods
  public async listCalendar() {
    return await this.calendarList.toArray()
  }

  public async getCalendar(id: number) {
    return await this.calendarList.get(id);
  }

  public async addCalendar(calendarItem: CalendarEvent) {
    return await this.calendarList.add(calendarItem);
  }

  public async editCalendar(calendarItem: CalendarEvent) {
    return await this.calendarList.update(calendarItem, calendarItem);
  }

  public async deleteCalendar(id: number) {
    return await this.calendarList.delete(id);
  }

  // note methods
  public async listNotes() {
    return await this.noteList.toArray()
  }

  public async getNote(id: number) {
    return await this.noteList.get(id);
  }

  public async addNote(noteItem: Note) {
    return await this.noteList.add(noteItem);
  }

  public async editNote(noteItem: Note) {
    return await this.noteList.update(noteItem, noteItem);
  }

  public async deleteNote(id: number) {
    return await this.noteList.delete(id);
  }
}

export const db = new HaplanDB();
