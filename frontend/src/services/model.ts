export interface CalendarEvent {
  id?: number;
  name: string;
  datetime: Date;
  details?: string;
  place?: string;
}

export interface Note {
  id?: number;
  name: string;
  text?: string;
}
