import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { db } from 'src/services/db';

@Component({
  selector: 'app-note-edit',
  templateUrl: './note-edit.component.html',
  styleUrls: ['./note-edit.component.scss']
})
export class NoteEditComponent implements OnInit {

  public name?: string;
  public text?: string;

  public errors?: string;

  constructor(@Inject(MAT_DIALOG_DATA) public id: number,public dialogRef: MatDialogRef<NoteEditComponent>) { }

  ngOnInit(): void {
    db.getNote(this.id).then(data => {
      this.name = data?.name;
      this.text = data?.text;
    });
  }

  edit() {
    if(!(this.name!!) || this.name.length < 2) {
      this.errors = "Název musí být delší jak 2 znaky";
      return;
    }
    db.editNote({id: this.id, name: this.name, text: this.text}).then(_ => {
      this.close();
    });
  }

  close() {
    this.dialogRef.close();
  }
}
