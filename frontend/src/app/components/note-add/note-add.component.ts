import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { db } from 'src/services/db';

@Component({
  selector: 'app-note-add',
  templateUrl: './note-add.component.html',
  styleUrls: ['./note-add.component.scss']
})
export class NoteAddComponent implements OnInit {

  public name?: string;
  public text?: string;

  public errors?: string;

  constructor(public dialogRef: MatDialogRef<NoteAddComponent>) { }

  ngOnInit(): void {

  }

  add() {
    if(!(this.name!!) || this.name.length < 2) {
      this.errors = "Název musí být delší jak 2 znaky";
      return;
    }
    db.addNote({name: this.name, text: this.text}).then(_ => {
      this.close();
    });
  }

  close() {
    this.dialogRef.close();
  }

}
