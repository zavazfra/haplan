import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { db } from 'src/services/db';

@Component({
  selector: 'app-calendar-detail',
  templateUrl: './calendar-detail.component.html',
  styleUrls: ['./calendar-detail.component.scss']
})
export class CalendarDetailComponent implements OnInit {

  public name?: string;
  public date?: Date;
  public place?: string;
  public details?: string;

  constructor(@Inject(MAT_DIALOG_DATA) public id: number, public dialogRef: MatDialogRef<CalendarDetailComponent>) { }

  ngOnInit(): void {
    db.getCalendar(this.id).then(data => {
      this.name = data?.name;
      this.date = data?.datetime;
      this.place = data?.place;
      this.details = data?.details;
    })
  }


  close() {
    this.dialogRef.close();
  }


}
