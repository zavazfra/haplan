import { FactoryTarget } from '@angular/compiler';
import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { db } from 'src/services/db';
import { CalendarEvent } from 'src/services/model';
import { CalendarAddComponent } from '../calendar-add/calendar-add.component';
import { CalendarDetailComponent } from '../calendar-detail/calendar-detail.component';
import { CalendarEditComponent } from '../calendar-edit/calendar-edit.component';

@Component({
  selector: 'app-calendar-view',
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.scss']
})
export class CalendarViewComponent implements OnInit {

  @ViewChild('content') content?: ElementRef;
  @ViewChildren('calendars') calendars?: QueryList<any>;

  calendarEvents: CalendarEvent[] = []

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.reload();
  }

  ngAfterViewInit(): void {
    this.calendars!!.changes.subscribe(() => {
      try {
        this.content!!.nativeElement.scrollTop = this.content!!.nativeElement.scrollHeight;
      } catch (err) {}
    });
  }

  private reload() {
    db.listCalendar().then(data => {
      this.calendarEvents = data.sort((a, b) => {
        return a.datetime.valueOf() - b.datetime.valueOf();
      });
    });
  }

  public deleteCalendar(id?: number) {
    db.deleteCalendar(id!).then(_ => {
      this.reload();
    });
  }

  public infoPrint(calendar: CalendarEvent) {
    let text = calendar.name.substring(0, 30);
    if(!!calendar.place && calendar.place.length > 0) {
      text+=" ("+calendar.place?.substring(0, 25)+")";
    }
    if(!!calendar.details && calendar.details.length > 0) {
      text += ", Detaily: "+calendar.details?.substring(0, 100);
    }
    return text;
  }

  old(date: Date): boolean {
    return date.valueOf() < Date.now();
  }

  public editCalendar(id?: number) {
    // show edit
    let dialogRef = this.dialog.open(CalendarEditComponent, {
      panelClass: 'dialog-design',
      height: 'auto',
      width: '600px',
      data: id
    })
    dialogRef.afterClosed().subscribe(o => {
      this.reload();
    });
  }

  public addCalendar() {
    // show add
    let dialogRef = this.dialog.open(CalendarAddComponent, {
      panelClass: 'dialog-design',
      height: 'auto',
      width: '600px',
    })
    dialogRef.afterClosed().subscribe(_ => {
      this.reload();
    });
  }

  public detailCalendar(id?: number) {
    this.dialog.open(CalendarDetailComponent, {
      panelClass: 'dialog-design',
      height: 'auto',
      width: '600px',
      data: id
    });
  }

}
