import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { db } from 'src/services/db';
import { Note } from 'src/services/model';
import { NoteAddComponent } from '../note-add/note-add.component';
import { NoteDetailComponent } from '../note-detail/note-detail.component';
import { NoteEditComponent } from '../note-edit/note-edit.component';

@Component({
  selector: 'app-note-view',
  templateUrl: './note-view.component.html',
  styleUrls: ['./note-view.component.scss']
})
export class NoteViewComponent implements OnInit {

  @ViewChild('content') content?: ElementRef;
  @ViewChildren('notes') notes?: QueryList<any>;
  noteList: Note[] = [];

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    this.reload();
  }

  ngAfterViewInit(): void {
    this.notes!!.changes.subscribe(() => {
      try {
        this.content!!.nativeElement.scrollTop = this.content!!.nativeElement.scrollHeight;
      } catch (err) {}
    });
  }

  private reload() {
    db.listNotes().then(list => {
      this.noteList = list;
    });
  }

  public detailNote(id: number) {
    this.dialog.open(NoteDetailComponent, {
      panelClass: 'dialog-design',
      height: 'auto',
      width: '600px',
      data: id
    })
  }

  public infoPrint(note: Note) {
    let text = note.name;
    if(note.text!! && note.text.length > 0) {
      text += (" Details: " + note.text);
    }
    return text;
  }

  public editNote(id: number) {
    let dialogRef = this.dialog.open(NoteEditComponent, {
      panelClass: 'dialog-design',
      height: 'auto',
      width: '600px',
      data: id
    })
    dialogRef.afterClosed().subscribe(_ => {
      this.reload();
    });
  }

  public deleteNote(id: number){
    db.deleteNote(id).then(_ => {
      this.reload();
    });
  }

  public addNote() {
    let dialogRef = this.dialog.open(NoteAddComponent, {
      panelClass: 'dialog-design',
      height: 'auto',
      width: '600px'
    })
    dialogRef.afterClosed().subscribe(o => {
      this.reload();
    });
  }

}
