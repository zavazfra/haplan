import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { db } from 'src/services/db';

@Component({
  selector: 'app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss']
})
export class NoteDetailComponent implements OnInit {

  public name?: string;
  public text?: string;

  constructor(@Inject(MAT_DIALOG_DATA) public id: number, public dialogRef: MatDialogRef<NoteDetailComponent>) { }

  ngOnInit(): void {
    db.getNote(this.id).then(data => {
      this.name = data?.name;
      this.text = data?.text;
    });
  }

  close() {
    this.dialogRef.close();
  }

}
