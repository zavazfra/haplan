import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { db } from 'src/services/db';

@Component({
  selector: 'app-calendar-add',
  templateUrl: './calendar-add.component.html',
  styleUrls: ['./calendar-add.component.scss']
})
export class CalendarAddComponent implements OnInit {

  public name: string = "";
  public date: Date|null = null;
  public place: string = "";
  public details: string = "";

  public errors: string = "";

  constructor(public dialogRef: MatDialogRef<CalendarAddComponent>) { }

  ngOnInit(): void {
  }

  add() {
    if(this.checkNote()) {
      db.addCalendar({name: this.name, datetime: this.date==null?new Date(Date.now()):
        this.date, place: this.place, details: this.details}).then(_ => {
        this.close();
      }, error => {
        alert(error);
      });
    }
  }

  checkNote(): boolean {
    if(this.name.length < 2) {
      this.errors = "Název musí být alespoň 2 znaky dlouhý";
      return false;
    }
    if(!(this.date!!)) {
      this.errors = "Datum nesmí být prázdný";
      return false;
    }
    return true;
  }

  close() {
    this.dialogRef.close();
  }

}
