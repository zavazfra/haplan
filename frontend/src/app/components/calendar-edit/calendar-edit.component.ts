import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { db } from 'src/services/db';
import { CalendarEvent } from 'src/services/model';

@Component({
  selector: 'app-calendar-edit',
  templateUrl: './calendar-edit.component.html',
  styleUrls: ['./calendar-edit.component.scss']
})
export class CalendarEditComponent implements OnInit {

  public name?: string;
  public date?: Date;
  public place?: string;
  public details?: string;

  public errors: string = "";

  constructor(@Inject(MAT_DIALOG_DATA) public id: number, public dialogRef: MatDialogRef<CalendarEditComponent>) { }

  ngOnInit(): void {
    db.getCalendar(this.id).then(data => {
      this.name = data?.name;
      this.date = data?.datetime;
      this.place = data?.place;
      this.details = data?.details;
    })
  }

  edit() {
    if(this.checkNote()) {
      db.editCalendar({id: this.id, name: (this.name==null?"":this.name), datetime: this.date, place: this.place, details: this.details} as CalendarEvent)
      .then(_ => {
        this.close();
      });
    }
  }

  checkNote(): boolean {
    if(!(this.name!!) || this.name.length < 2) {
      this.errors = "Název musí být alespoň 2 znaky dlouhý";
      return false;
    }
    if(!(this.date!!)) {
      this.errors = "Datum nesmí být prázdný";
      return false;
    }
    return true;
  }

  close() {
    this.dialogRef.close();
  }

}
