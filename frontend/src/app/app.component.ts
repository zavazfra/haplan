import { Component, ElementRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { db } from 'src/services/db';
import { CalendarViewComponent } from './components/calendar-view/calendar-view.component';
import { NoteViewComponent } from './components/note-view/note-view.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private view: 'CALENDAR' | 'NOTES' = 'NOTES'

  @ViewChild("display", {static: true, read: ViewContainerRef }) center!: ViewContainerRef;

  constructor() {
    db.listCalendar().then((data) => {
      console.log(data);
    });
  }

  ngOnInit(): void {
    console.log(this.center)
    this.switchToCalendar();
  }

  switchToCalendar() {
    if(this.view != 'CALENDAR') {
      this.center.clear()
      this.center.createComponent(CalendarViewComponent)
      this.view = 'CALENDAR'
    }
  }

  switchToNotes() {
    if(this.view != 'NOTES') {
      this.center.clear()
      this.center.createComponent(NoteViewComponent)
      this.view = 'NOTES'
    }
  }


}
